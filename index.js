//console.log("Hello 204!");

/*
	Repetition Control Structure(Loops)
		Loops are one of the most important feature that a programming must have.
		It lets us execute a code repeatedly in a pre-set number or maybe forever.

*/
/*
	Mini Activity: 8:03PM
		Create a function named greeting and display a "Hello, my World!" using console.log inside the function.

		Invoke the greeting() function 20 times.
	
		Send the screenshot in our batch hangouts.
*/

function greeting() {
	console.log("Hello, my World!");
}


// let count = 20;

// // While the value of count is not equal to 0
// while (count !== 0) {

// 	//The current value of count is printed out
// 	console.log("This is printed inside the loop: " + count);
// 	greeting();
	

// 	// Decreases the value of count by 1 after every iteration to stop the loop when it reaches 0
// 	// Loops occupy a significant amount of memory space in our devices
// 	// Make sure that expressions/conditions in loops have their corresponding increment/decrement operators to stop the loop
// 	// Forgetting to include this in loops will make our applications run an infinite loop which will eventually crash our devices
// 	// After running the script, if a slow response from the browser is experienced or an infinite loop is seen in the console quickly close the application/browser/tab to avoid this
// 	count--;
// }

/*
	While Loop 	
		- takes in an expression/condition
		- Expressions are any unit of code that can be evaluated to a value
		- If the condition evaluates to true, the statements inside the code block will be executed
		- A statement is a command that the programmer gives to the computer
		- A loop will iterate a certain number of times until an expression/condition is met
		- "Iteration" is the term given to the repetition of statements


	Syntax:
		while(expression/condition) {
			statement
		}

*/
/*
	Mini Activity: 8:22PM
		The while loop should only display the number 1-5.
		Correct the following loop.

		let x = 0;

		while(x < 1) {
			console.log(x);
			x--;
		} 
	
*/

// let x = 1 ;

// while(x <= 5) {
// 	console.log(x);
// 	x++;
// } 


/*

	Do While Loop
		-A do-while loop works a lot like the while loop. But unlike while loops, do-while loops guarantees that the code will be executed at least once.

	Syntax:
		do {
			statement
		} while (expression/condition)
	
	- The "Number" function works similarly to the "parseInt" function
*/

// let number = Number(prompt("Give me a number:"));

// do {

// 	//The current value of number is printed out
// 	console.log("Do While: "+ number); //Do While: 1
	
// 	//Increases the value of number by 1 after every iteration to stop the loop when it reaches 10 or greater
// 	number += 1;

// 	//Providing a number of 10 or greater will run the code block once and will stop the loop
// } while (number < 10)

/*
	FOR LOOPS
		- more flexible than while and do-while loop
	It consists of three parts:
	        1. The "initialization" value that will track the progression of the loop.
	        2.  The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
	        3. The "finalExpression" indicates how to advance the loop.


	Syntax:
		for(initialization; expression/condition; finalExpression) {
			statement
		}
*/

// let number = Number(prompt("Give me a number:"));

// for (let count = number; count <= 20; count++) {

// 	//The current value of count is printed out
// 	console.log("For Loop: " + count)
// }

//Can we use for loops in string?
// let myString = "eric";

// //Accessing element of a string
// // Individual characters of a string may be accessed using it's index number
// // The first character in a string corresponds to the number 0, the next is 1 up to the nth number
// console.log(myString[0]); //e
// console.log(myString[1]);//r

// // Characters in strings may be counted using the .length property
// // Strings are special compared to other data types in that it has access to functions and other pieces of information another primitive data type might not have
// console.log(myString.length); //4


// Will create a loop that will print out the individual letters of the myString variable
// for(let i = 0; i < myString.length; i++) {

// 	// The current value of myString is printed out using it's index value
// 	console.log(myString[i]);
// }


// Create a string named "myName" with a value of your name
// let myName = "alExandrO";

// for (let i = 0; i < myName.length; i++) {

// 	console.log(myName[i]);

// 	// If the character of your name is a vowel letter, instead of displaying the character, display "Vowel!"
// 	// The ".toLowerCase()" function/method will change the current letter being evaluated in a loop to a lowercase letter ensuring that the letters provided in the expressions below will match
	  

// 	if(
// 		myName[i].toLowerCase() === "a" ||
// 		myName[i].toLowerCase() === "e" ||
// 		myName[i].toLowerCase() === "i" ||
// 		myName[i].toLowerCase() === "o" ||
// 		myName[i].toLowerCase() === "u" 
// 	) {

// 		// If the letter in the name is a vowel, it will print "Vowel!"
// 		console.log("Vowel!");

// 	} else {

// 		// Print in the console all non-vowel characters in the name
// 		console.log(myName[i]);
// 	}

// }


//Continue and Break Statements
/*
	"Continue" statement allows the code to go to the next iteration of the loop without finishing the execution of all the statements in a code block

	"Break" statement is used to terminate the current loop once the match has been found

*/

for (let count = 0; count <= 20; count++){
	
	// if remainder is equal to 0
	if(count % 2 === 0) {
		console.log("Even Number");

		// Tells the code to continue to the next iteration of the loop
		// This ignores all statements located after the continue statement;
		continue;
	}

	// The current value of number is printed out if the remainder is not equal to 0
	console.log("Continue and Break: " + count);

	// If the current value of count is greater than 10
	if(count > 10) {

		// Tells the code to terminate/stop the loop even if the expression/condition of the loop defines that it should execute so long as the value of count is less than or equal to 20
		// number values after 10 will no longer be printed
		break;
	}
}


let name = "alexandro";

for (let i = 0; i < name.length; i++)
{
	
	if (name[i].toLowerCase() === "a") 
	{
		console.log("Continue to the next iteration");
		continue;
	}

	console.log(name[i]);

	if (name[i] === "d") 
	{
		break;
	}
}